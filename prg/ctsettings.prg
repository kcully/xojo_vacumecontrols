DEFINE CLASS ctSettings AS Custom

*!**********************************
*!* Program:
*!* Author: CULLY Technologies, LLC
*!* Date: 03/24/11 02:11:01 PM
*!* Copyright:
*!* Description:
*!* Revision Information:
FUNCTION GetSetting( tcSetting, tcValue )
	LOCAL lcRetVal
	lcRetVal = []
	IF THIS.CreateSettingsTable()	
		SELECT tSettings
		IF SEEK( ALLTRIM(tcSetting), "tSettings", "cSetting")
			lcRetVal = ALLTRIM( tSettings.cValue )
		ELSE
			tcValue = TRANSFORM( tcValue )
			INSERT INTO tSettings (cSetting, cValue) VALUES (tcSetting, tcValue)
			lcRetVal = ALLTRIM( tSettings.cValue )
		ENDIF
	ENDIF
	
RETURN lcRetVal


*!**********************************
*!* Program:
*!* Author: CULLY Technologies, LLC
*!* Date: 04/23/11 10:50:43 AM
*!* Copyright:
*!* Description:
*!* Revision Information:
FUNCTION SaveSetting( tcSetting, tcValue )
	LOCAL llRetVal
	llRetVal = .F.
	
	IF THIS.CreateSettingsTable()
		lnOrigArea = SELECT()
		tcValue = TRANSFORM( tcValue )

		IF SEEK( ALLTRIM(tcSetting), "tSettings", "cSetting" )
			REPLACE cValue WITH tcValue IN tSettings
			llRetVal = .T.
		ELSE
			INSERT INTO tSettings (cSetting, cValue) VALUES (tcSetting, tcValue)
			llRetVal = .T.
		ENDIF
		
		SELECT (lnOrigArea)
	ENDIF
	
	
RETURN llRetVal

*!**********************************
*!* Program:
*!* Author: CULLY Technologies, LLC
*!* Date: 04/23/11 10:52:02 AM
*!* Copyright:
*!* Description:
*!* Revision Information:
FUNCTION CreateSettingsTable()
	LOCAL llRetVal
	llRetVal = .F.
	
	DO CASE
		CASE USED("tSettings")
			llRetVal = .T.
		CASE FILE("tSettings.dbf")
			USE tSettings IN 0 SHARED AGAIN
		OTHERWISE
			SET STEP ON 
			TRY
				CREATE TABLE tSettings (cSetting C(100), cValue C(254), PK I AUTOINC)
				SELECT tSettings
				INDEX ON ALLTRIM(cSetting) TAG cSetting CANDIDATE
				USE IN tSettings
				USE tSettings IN 0 SHARED AGAIN
			CATCH
			ENDTRY
			llRetVal = USED("tSettings")
	ENDCASE
	
RETURN llRetVal

ENDDEFINE
