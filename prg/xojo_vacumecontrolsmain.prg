CLOSE DATABASES ALL
USE data\tControlTemplate IN 0 EXCLUSIVE

SET PROCEDURE TO VFPtoXojoMain ADDITIVE
PUBLIC oConverter, oVacume
oConverter = CREATEOBJECT( "VFPToXojoConverter" )
oVacume = CREATEOBJECT( "XojoVacume" )

&& SET STEP ON 

DO FORM Xojo_VacumeControls


*==============================================================
*==============================================================
*==============================================================
*==============================================================
DEFINE CLASS XojoVacume AS Custom

cControlQueue = []
nControlDepth = 0

*!* ====================================
*!* Program:
*!* Author: Kevin Cully
*!* Date: 04/02/12 10:45:42 AM
*!* Copyright:
*!* Description:
*!* Revision Information:
*!*		KJC 10/7/2013 - Change to the Xojo file names.
FUNCTION VacumeFile( tcXojoFile AS String )
LOCAL llRetVal, lnPrevStart, lnPrevEnd, lc1stWord, lc2ndWord, lcType
llRetVal = .F.

	lnLines = oConverter.ProgramIntoCursor( tcXojoFile, "c_XojoFile")
	IF lnLines > 0 THEN
		lcType = LOWER( ALLTRIM( JUSTEXT( tcXojoFile ) ) )
		
		DO CASE
			CASE lcType = "xojo_code"
				THIS.AddControlsWithRecursion( "c_XojoFile", 1 )
			CASE lcType = "xojo_window"
				THIS.AddControlsWithRecursion( "c_XojoFile", 1 )
			CASE lcType = "xojo_project"
				THIS.AddProject( "c_XojoFile" )
			OTHERWISE
				SET STEP ON 
		ENDCASE
	ELSE
		SET STEP ON 
	ENDIF
	=CloseDBF("c_XojoFile")

RETURN llRetVal
ENDFUNC


*!* ====================================
*!* Program:
*!* Author: Kevin Cully
*!* Date: 04/02/12 01:58:29 PM
*!* Copyright:
*!* Description:
*!* Revision Information:
FUNCTION  AddControls( tcSourceCursor )
	LOCAL llRetVal
	llRetVal = .F.
	
	IF USED( tcSourceCursor )
		SELECT (tcSourceCursor)
		SCAN
			lc1stWord = UPPER( GETWORDNUM( cCode, 1, " " ) )
			lc2ndWord = UPPER( GETWORDNUM( cCode, 2, " " ) )
			&& DEBUGOUT TRANSFORM(RECNO()) + " of " + TRANSFORM( RECCOUNT()) + " :: " + lc1stWord + " " + lc2ndWord TIMEOUT 0.1
			DO CASE
				CASE INLIST(lc1stWord, "#TAG") AND LEFT( lc2ndWord, 3) # "END"
					lcType = lc2ndWord
					&& We're in a control that wraps another control.  We need sections that are BEGIN and later END sections.
					lcType = lcType + "_BEGIN"
					THIS.ControlPush( lcType )
					lnStartRecord = RECNO()
					llInAControl = .T.
				CASE INLIST( lc1stWord, "#TAG" ) AND LEFT( lc2ndWord, 3) = "END"
					THIS.Add1Control( lcType, lnStartRecord, RECNO() )
					THIS.ControlPop( lcType )
					llInAControl = .F.
					lnStartRecord = RECNO() + 1
				CASE lc1stWord = "BEGIN" AND NOT THIS.PrevRecordIsPoundTag()
					THIS.Add1Control( THIS.GetLastControl(), lnStartRecord, RECNO()-1 )
					lcType = lc2ndWord
					THIS.ControlPush( lcType )
					lnStartRecord = RECNO()
					llInAControl = .T.
				CASE INLIST( lc1stWord, "END" ) AND NOT THIS.NextRecordIsPoundTag()
					THIS.Add1Control( lcType, lnStartRecord, RECNO() )
					THIS.ControlPop( lcType )
					llInAControl = .F.
					lnStartRecord = RECNO() + 1
				OTHERWISE
					&& We're on a normal record so just let the SCAN move to the next record.
					&& This also applies of the lc1stWord is "END" but the next record is the #TAG ENDsomething.
			ENDCASE
			
		ENDSCAN
	ELSE
		SET STEP ON 
	ENDIF
RETURN llRetVal
ENDFUNC

*!* ====================================
*!* Program:
*!* Author: Kevin Cully
*!* Date: 04/23/12 01:58:29 PM
*!* Copyright:
*!* Description:
*!* Revision Information:
*!*		KJC 10/11/2013 - Changed lc1stWord to be the ALLTRIM() so the INLIST would work.
*!*		KJC 10/11/2013 - Changed lc1stWord to be reference cCode1 instead of just cCode.  We're getting special characters in lc1stWord.
FUNCTION  AddControlsWithRecursion( tcSourceCursor, tnLevel )
	LOCAL llRetVal, lnStartRecord, lnStartRecord, lc1stWord, lc2ndWord, lcType
	llRetVal = .F.
	
	IF USED( tcSourceCursor ) AND tnLevel < 10
		SELECT (tcSourceCursor)
		lnStartRecord = RECNO()
		DO WHILE NOT EOF()
			lc1stWord = ALLTRIM( UPPER( GETWORDNUM( cCode1, 1, " " ) ) )
			lc1stWord = oConverter.CleanString( lc1stWord, "ANPS" )
			IF INLIST(lc1stWord, "#TAG", "BEGIN", "END")
				&& SET STEP ON 
				lc2ndWord = UPPER( GETWORDNUM( cCode, 2, " " ) )
				DEBUGOUT TRANSFORM(RECNO()) + " of " + TRANSFORM( RECCOUNT()) + " :: " + TRANSFORM(lnStartRecord) + "- " + lc1stWord + " " + lc2ndWord TIMEOUT 0.1
				DO CASE
					CASE lc1stWord = "BEGIN"
						IF lnStartRecord # RECNO()
							THIS.Add1Control( lcType + "_BEGIN", lnStartRecord, RECNO()-1 )
							THIS.AddControlsWithRecursion( tcSourceCursor, tnLevel + 1 )
							&& We're back from the recursive call do we begin the "END" section of a control.
							lcType = lcType + "_END"	
						ELSE
							lcType = lc2ndWord
						ENDIF
						THIS.ControlPush( lcType )
						lnStartRecord = RECNO()
					CASE INLIST( lc1stWord, "END" )
						THIS.Add1Control( lcType, lnStartRecord, RECNO() )
						THIS.ControlPop( lcType )
						EXIT
					OTHERWISE
						SET STEP ON 
				ENDCASE
			ENDIF
			SKIP 1
		ENDDO
	ELSE
		SET STEP ON 
	ENDIF
RETURN llRetVal
ENDFUNC


*!* ====================================
*!* Program:
*!* Author: Kevin Cully
*!* Date: 04/02/12 01:47:48 PM
*!* Copyright:
*!* Description:
*!* Revision Information:
FUNCTION Add1Control( tcType, tnStartRec, tnEndRec )
	LOCAL llRetVal, lnOrigArea, lnOrigRec
	llRetVal = .F.
	lnOrigArea = SELECT()
	lnOrigRec = RECNO()
	DEBUGOUT "Adding " + tcType + ": records " + TRANSFORM(tnStartRec) + " through " + TRANSFORM(tnEndRec)

	SELECT tControlTemplate
	cNewType = LEFT( tcType + SYS(2015), 20 )	&& Length of the field is 20 characters.
	&& Rename old type to a new type, just for a moment before they're deleted.
	REPLACE ALL cType WITH cNewType FOR ALLTRIM(UPPER(cType)) == ALLTRIM(UPPER(tcType))
	&& Delete the old type of records.
	GO TOP
	&& SET STEP ON 
	DELETE FOR ALLTRIM(UPPER(cType)) == ALLTRIM(UPPER(cNewType)) IN tControlTemplate
	PACK IN tControlTemplate
	
	SELECT LOWER(tcType) AS cType, RECNO() AS nOrder, cCode FROM c_XojoFile ;
		WHERE BETWEEN( RECNO(), tnStartRec, tnEndRec ) ;
		INTO CURSOR c_Temp NOFILTER
	
	SELECT c_Temp
	SCAN
		loProperty = oConverter.PropertyToObject( c_Temp.cType, c_Temp.nOrder, c_Temp.cCode )
		INSERT INTO tControlTemplate FROM NAME loProperty
	ENDSCAN

	=CloseDBF("c_Temp")
	
	&& SET STEP ON 
	SELECT (lnOrigArea)
	GO (lnOrigRec)
RETURN llRetVal
ENDFUNC


*!* ====================================
*!* Program:
*!* Author: Kevin Cully
*!* Date: 04/24/12 02:37:38 PM
*!* Copyright:
*!* Description:
*!* Revision Information:
FUNCTION AddProject( tcSourceCursor )
	LOCAL llRetVal
	llRetVal = .F.
	SELECT (tcSourceCursor)
	LOCATE FOR "TYPE=" = UPPER( ALLTRIM( LEFT( cCode1, 5 ) ) )
	IF FOUND()
		lcType = GETWORDNUM( cCode1, 2, "=" )
		THIS.Add1Control( "PROJECT_" + lcType, 1, RECCOUNT())
	ELSE
		SET STEP ON 
	ENDIF
RETURN llRetVal
ENDFUNC

*!* ====================================
*!* Program:
*!* Author: Kevin Cully
*!* Date: 04/02/12 01:48:18 PM
*!* Copyright:
*!* Description:
*!* Revision Information:
FUNCTION NextRecordIsPoundTag()
LOCAL llRetVal, lnOrigRec, lc1stWord
llRetVal = .F.
	lnOrigRec = RECNO()
	IF NOT EOF()
		SKIP
		IF NOT EOF()
			lc1stWord = UPPER( GETWORDNUM( cCode, 1, " " ) )
			llRetVal = ( lc1stWord = "#TAG" )
		ENDIF
	ENDIF
	GO (lnOrigRec)
RETURN llRetVal
ENDFUNC

*!* ====================================
*!* Program:
*!* Author: Kevin Cully
*!* Date: 04/04/12 01:48:18 PM
*!* Copyright:
*!* Description:
*!* Revision Information:
FUNCTION PrevRecordIsPoundTag()
	LOCAL llRetVal, lnOrigRec, lc1stWord
	llRetVal = .F.
	lnOrigRec = RECNO()
	IF NOT ( BOF() OR RECNO()=1 )
		SKIP -1
		lc1stWord = UPPER( GETWORDNUM( cCode, 1, " " ) )
		llRetVal = ( lc1stWord = "#TAG" )
	ENDIF
	GO (lnOrigRec)
RETURN llRetVal
ENDFUNC

*!* ====================================
*!* Program: ControlPush
*!* Author: Kevin Cully
*!* Date: 04/04/12 02:49:21 PM
*!* Copyright:
*!* Description:
*!* Revision Information:
FUNCTION ControlPush( tcControlType )
	LOCAL llRetVal
	llRetVal = .T.
	THIS.cControlQueue = THIS.cControlQueue ;
		+ IIF(EMPTY(THIS.cControlQueue), "", " " ) ;
		+ ALLTRIM( tcControlType )
	THIS.nControlDepth = THIS.nControlDepth + 1
RETURN llRetVal
ENDFUNC

*!* ====================================
*!* Program: ControlPop
*!* Author: Kevin Cully
*!* Date: 04/04/12 02:49:21 PM
*!* Copyright:
*!* Description:
*!* Revision Information:
FUNCTION ControlPop( tcControlType )
	LOCAL llRetVal
	llRetVal = .T.
	lnWordCount = GETWORDCOUNT( THIS.cControlQueue )
	IF lnWordCount <= 1
		THIS.cControlQueue = []
	ELSE
		&& Take off one control type from the queue...
		THIS.cControlQueue = LEFT( THIS.cControlQueue, RAT( THIS.cControlQueue, " " ) )
		THIS.nControlDepth = THIS.nControlDepth - 1
	ENDIF
RETURN llRetVal
ENDFUNC

*!* ====================================
*!* Program: ControlPop
*!* Author: Kevin Cully
*!* Date: 04/04/12 02:49:21 PM
*!* Copyright:
*!* Description:
*!* Revision Information:
FUNCTION GetLastControl( )
	LOCAL lcRetVal
	lcRetVal = []
	lnWordCount = GETWORDCOUNT( THIS.cControlQueue )
	IF lnWordCount <= 1
		lcRetVal = THIS.cControlQueue
	ELSE
		&& Get the last word from the list.
		lcRetVal = GETWORDNUM( THIS.cControlQueue, lnWordCount )
	ENDIF
RETURN lcRetVal
ENDFUNC




ENDDEFINE
*==============================================================
*==============================================================
*==============================================================
