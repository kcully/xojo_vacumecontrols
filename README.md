# README #

The Xojo_VacumeControls project is meant to be complimentary to the VFPToXojo project.  This application is built in VFP9.  The binary VFP projects, screens have been converted to source control format using the FoxBin2PRG program from http://vfpx.codeplex.com.

### What is this repository for? ###

* Quick summary: Xojo_VacumeControls project is meant to help keep up with the latest versions of Xojo and the project that they create.  The VFPToXojo project is meant to output a valid Xojo project from what (little) it can convert from VFP Projects, forms, and programs.  To make it as easy to keep current, I have created this program that 'vacumes' up a valid Xojo project, and puts it into a tempate form of data.  The VFPToXojo project will use this template data to convert properties, methods, and events into Xojo format.
* Version: Alpha 0.1

### How do I get set up? ###

* Copy/download/pull all files to a VFP friendly directory. Example: c:\vfp\xojo_vacumecontrols; c:\vfp\xojo_vacumecontrols\data; c:\vfp\xojo_vacumecontrols\form; c:\vfp\xojo_vacumecontrols\prg
* Run the "make_table_tcontroltemplate.prg" from within VFP9.  This should create data\tcontroltemplate.dbf and data\tcontroltemplate.cdx.
* Run the FoxBin2PRG program to convert the xojo_vacumecontrols.pj2 back to a PJX project set, plus the form\xojo_vacumecontrols.sc2 back to a SCX form set.
* Build the xojo_vacumecontrols project into an EXE
* Run the program and point it to a current Xojo project that is in source code format (*.xojo_project) so that it vacumes in current Xojo settings.
* If all is successful, run the VFPToXojo converter against this same tcontroltemplate table to convert your VFP application to Xojo.

### Contribution guidelines ###

* This application was really created as a "proof of concept" application. It is not really meant to convert complex VFP projects to Xojo.
* If you're really passionate about the application, please test thoroughly and then push changes back to the repository.

### Who do I talk to? ###

* Kevin Cully is the Repo owner but don't contact him for support.  Roll up your sleeves, get your debugging hat on, push forward, and have fun.